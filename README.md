### Dependencies
1. Postgres 12
2. Go 1.13
### Configure
## Local db setup
1. Install postgresql locally
2. Create dev user/db.
```
sudo su postgres
cat devdb.sql > psql
```
Default connection string is postgres://uj:uj@localhost/uj

## Normal setup
1. `cat create.sql | psql < connection string >` (this wipes the specified db and creates tables)

### Run
2. `export $(cat .env)`
3. `go run cmd/microjournal/main.go`

